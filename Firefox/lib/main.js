var localStorage = require("sdk/simple-storage");
if (localStorage.storage.blockedMedia === undefined) {
	localStorage.storage.blockedMedia = [];
}

var data = require("sdk/self").data;
var recent = {
	"type": "Audio",
	"url": "",
	"word": ""
};

///////////////////////////////////////////////// Action button & panel /////////////////////////////////////////////////

var { ToggleButton } = require("sdk/ui/button/toggle");
var tabs = require("sdk/tabs");

var button = ToggleButton({
	id: "edit-panel",
	label: "Edit Media Blocklist",
	icon: {
		"16": "./icon-16.png",
		"32": "./icon-32.png",
		"64": "./icon-64.png"
	},
	onClick: handleChange
});

var panel = require("sdk/panel").Panel({
	contentURL: require("sdk/self").data.url("panel.html"),
	contentScriptFile: [data.url("jquery-2.1.0.min.js"), data.url("panel.js")],
	contentScriptOptions: {
		blockedMedia: localStorage.storage.blockedMedia
	},
	width: 326,
	height: 396,
	onHide: handleHide
});

function handleChange(state) {

	if (state.checked) {
		panel.show({
			position: button
		});
	}

}

function handleHide() {

	button.state('window', {
		checked: false
	});

}

panel.on('show', function() {
	panel.port.emit('opened', localStorage.storage.blockedMedia);
});

panel.port.on("removeBlock", function(args) {
	removeBlock(args.type, args.url, args.word);
	for (var i = 0; i < ports.length; i++) {
		ports[i].emit('removeBlock', args.url);
	}
});

panel.port.on("removeAll", function() {
	localStorage.storage.blockedMedia.length = 0;
	for (var i = 0; i < ports.length; i++) {
		ports[i].emit('resetPage');
	}
});

function getWordForRecent() {

	for (var i = 0; i < ports.length; i++) {
		ports[i].emit('retrieveWord');
	}

}


function notify(message) {

	for (var i = 0; i < ports.length; i++) {
		ports[i].emit('notify', message);
	}

}

function toggleRecent(state) {

	for (var i = 0; i < ports.length; i++) {
		ports[i].emit('toggleAudio', {
			"url": recent.url,
			"state": state
		});
	}

}

///////////////////////////////////////////////// Action button toggle /////////////////////////////////////////////////

require("sdk/tabs").on("ready", logURL);

function logURL(tab) {

	if (tab.url.indexOf("memrise.com") !== -1) {
		toggleActionButton(false);
	} else {
		toggleActionButton(true);
	}

}

function toggleActionButton(state) {

	button.state("tab", {
		disabled: state
	});

}

///////////////////////////////////////////////// Action button toggle /////////////////////////////////////////////////

var { Hotkey } = require("sdk/hotkeys");

var blockAudio = Hotkey({
	combo: "control-shift-x",
	onPress: function() {
		blockRecent();
	}
});
var unblockAudio = Hotkey({
	combo: "control-shift-z",
	onPress: function() {
		unBlockRecent();
	}
});

function blockRecent() {

	if (recent.url.length > 0 && recent.word.length > 0) {
		if (!doesExist(recent.url)) {
			addBlock(recent.type, recent.url, recent.word);
			toggleRecent(false);
		}
	}

}

function unBlockRecent() {

	if (recent.url.length > 0 && recent.word.length > 0) {
		if (doesExist(recent.url)) {
			removeBlock(recent.type, recent.url, recent.word);
			toggleRecent(true);
		}
	}

}

///////////////////////////////////////////////// Page Check /////////////////////////////////////////////////

var pageMod = require("sdk/page-mod");

var ports = [];
pageMod.PageMod({
	include: "*.memrise.com",
	contentScriptWhen: "ready",
	contentScriptFile: [data.url("jquery-2.1.0.min.js"), data.url("notify.min.js"), data.url("share.js"), data.url("check.js")],
	contentScriptOptions: {
		blockedMedia: localStorage.storage.blockedMedia
	},
	onAttach: function(worker) {
		worker.on('pageshow', function() {
			ports.push(worker.port);
		});
		worker.on('pagehide', function() {
			var index = ports.indexOf(worker.port);
			if (index !== -1) ports.splice(index, 1);
		});
		worker.on('detach', function() {
			var index = ports.indexOf(worker.port);
			if (index !== -1) ports.splice(index, 1);
		});
		worker.port.on("addRecent", function(word) {
			recent.word = word;
		});
		worker.port.on("retrieveList", function() {
			for (var i = 0; i < ports.length; i++) {
				ports[i].emit('updateList', localStorage.storage.blockedMedia);
			}
		});
		worker.port.on("setRecent", function(args) {
			recent.type = args.type;
			recent.url = args.url;
			recent.word = args.word;
		});
	}

});

///////////////////////////////////////////////// Context Menu /////////////////////////////////////////////////

var contextMenu = require("sdk/context-menu");

var blockAudioItem = contextMenu.Item({
	label: "Block Audio",
	context: [contextMenu.URLContext(["*.memrise.com"]), contextMenu.SelectorContext("a[href]"), contextMenu.PredicateContext(function(context) {
		var url = context["linkURL"];
		if (url.indexOf("uploads") !== -1 && url.indexOf("audio") !== -1) {
			if (doesExist(url) == true) {
				return false;
			} else {
				return true;
			}
		}
	})],
	contentScriptFile: [data.url("jquery-2.1.0.min.js"), data.url("share.js"), data.url("action.js")],
	onMessage: function(args) {
		addBlock(args.type, args.url, args.word);
	}
});

var unBlockAudioItem = contextMenu.Item({
	label: "Unblock Audio",
	context: [contextMenu.URLContext(["*.memrise.com"]), contextMenu.SelectorContext("a[href]"), contextMenu.PredicateContext(function(context) {
		var url = context["linkURL"];
		if (url.indexOf("uploads") !== -1 && url.indexOf("audio") !== -1) {
			if (doesExist(url) == true) {
				return true;
			} else {
				return false;
			}
		}
	})],
	contentScriptFile: [data.url("jquery-2.1.0.min.js"), data.url("share.js"), data.url("action.js")],
	onMessage: function(args) {
		removeBlock(args.type, args.url, args.word);
	}
});

var blockImageItem = contextMenu.Item({
	label: "Block Image",
	context: [contextMenu.URLContext(["*.memrise.com"]), contextMenu.SelectorContext("img"), contextMenu.PredicateContext(function(context) {
		var src = context["srcURL"];
		if (src.indexOf("uploads") !== -1 && src.indexOf("images") !== -1) {
			if (doesExist(src) == true) {
				return false;
			} else {
				return true;
			}
		}
	})],
	contentScriptFile: [data.url("jquery-2.1.0.min.js"), data.url("share.js"), data.url("action.js")],
	onMessage: function(args) {
		addBlock(args.type, args.url, args.word);
	}
});

var unBlockImageItem = contextMenu.Item({
	label: "Unblock Image",
	context: [contextMenu.URLContext(["*.memrise.com"]), contextMenu.SelectorContext("img"), contextMenu.PredicateContext(function(context) {
		var src = context["srcURL"];
		if (src.indexOf("uploads") !== -1 && src.indexOf("images") !== -1) {
			if (doesExist(src) == true) {
				return true;
			} else {
				return false;
			}
		}
	})],
	contentScriptFile: [data.url("jquery-2.1.0.min.js"), data.url("share.js"), data.url("action.js")],
	onMessage: function(args) {
		removeBlock(args.type, args.url, args.word);
	}
});

///////////////////////////////////////////////// Block/Unblock & Storage Actions /////////////////////////////////////////////////

function addBlock(type, url, word) {

	var entry = {
		"type": type,
		"url": url,
		"word": word
	};

	if (doesExist(url) == false) {
		localStorage.storage.blockedMedia.push(entry);
		var message = entry.type + " blocked for \"" + entry.word + "\"";;
		notify(message);

	}

}

function removeBlock(type, url, word) {

	for (i = 0; i < localStorage.storage.blockedMedia.length; i++) {
		var entry = localStorage.storage.blockedMedia[i];
		if (entry.url == url) {
			localStorage.storage.blockedMedia.splice(i, 1);
			break;
		}
	}

	if (type.length > 0) {
		var message = entry.type + " unblocked for \"" + entry.word + "\"";
		notify(message);
	}

}

function doesExist(url) {

	if (localStorage.storage.blockedMedia != null) {
		for (i = 0; i < localStorage.storage.blockedMedia.length; i++) {
			var entry = localStorage.storage.blockedMedia[i];
			if (entry.url == url) {
				return true;
				break;
			}
		}
		return false;
	} else {
		return false;
	}

}

///////////////////////////////////////////////// URL Intercept /////////////////////////////////////////////////

var {
	Cc, Ci, Cr
} = require("chrome");

var httpRequestObserver = {
	observe: function(subject, topic, data) {

		if (topic == "http-on-modify-request") {
			var httpChannel = subject.QueryInterface(Ci.nsIHttpChannel);
			var requestURI = httpChannel.URI.spec;
		
			if (requestURI.indexOf("uploads") !== -1 && requestURI.indexOf("audio") !== -1) {
				recent.type = "Audio";
				recent.url = requestURI;
				getWordForRecent();
			}
		}
	}
};

var observerService = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
observerService.addObserver(httpRequestObserver, "http-on-modify-request", false);
