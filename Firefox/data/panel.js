var blockedMedia = self.options.blockedMedia;

self.port.on('opened', function(data) {
	blockedMedia = data;
	main();
});

function main() {

	$('#tableContent').empty();

	var tbody = $('#tableContent'),
		props = ["*", "Media"];
	$.each(blockedMedia, function(i, entry) {
		var tag = "<div class='type" + entry.type + "' url='" + entry.url + "' word='" + entry.word + "' ></div>";
		var dir = oddOrEven(i);
		var rowType = "<tr>";
		if (dir == "odd") {
			rowType = "<tr class='pure-table-odd'>";
		}

		var tr = $(rowType);
		$("<td class='mediaType'>").html(tag).appendTo(tr);
		$("<td class='mediaData'>").html(entry.word).appendTo(tr);
		tbody.append(tr);
	});
	
	for(var i = blockedMedia.length; i<8; i++){
		var dir = oddOrEven(i);
		var rowType = "<tr>";
		if (dir == "odd") {
			rowType = "<tr class='pure-table-odd'>";
		}
		var tag = "<div class='typeBlank'></div>";
		var tr = $(rowType);
		$("<td class='mediaType'>").html(tag).appendTo(tr);
		$("<td class='mediaData'>").appendTo(tr);
		tbody.append(tr);
	}

	$(".typeAudio").mousedown(function() {
		var url = $(this).attr("url");
		var word = $(this).attr("word");
		remove(url);
		self.port.emit('removeBlock', {"type": "Audio", "url": url, "word": word });
		main();
	});
	
	$(".typeAudio").mouseenter(function() {
		var url = $(this).attr("url");
		var snd = new Audio(url); // buffers automatically when created
		snd.play();
	});
	
	$(".typeImage").mousedown(function() {
		var url = $(this).attr("url");
		var word = $(this).attr("word");
		self.port.emit('removeBlock', {"type": "Image", "url": url, "word": word });
		remove(url);
		main();
	});

	$(".removeAction").mousedown(function() {
		blockedMedia = [];
		self.port.emit('removeAll');
		main();
	});

}

function remove(url) {
	for (i = 0; i < blockedMedia.length; i++) {
		var entry = blockedMedia[i];
		if (entry.url == url) {
			blockedMedia.splice(i, 1);
			break;
		}
	}
}

function oddOrEven(x) {
	return (x & 1) ? "odd" : "even";
}

main();