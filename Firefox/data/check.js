var blockedMedia = self.options.blockedMedia;
var target = document.querySelector('#boxes');

var observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		self.port.emit('retrieveList');
		checkMediaStatus();
	});
});
var config = {
	attributes: true,
	childList: true,
	characterData: true
};

if(target != undefined){
	observer.observe(target, config);
}


self.port.on('removeBlock', function(url) {
	removeBlock(url);
});

self.port.on('updateList', function(updatedList) {
	blockedMedia = updatedList;
});

self.port.on('resetPage', function() {
	self.port.emit('retrieveList');
    blockedMedia = new Array();
	resetAll();
});

self.port.on('notify', function(message) {

	$.notify.addStyle('blocked', {
	  html: "<div><span data-notify-text/></div>",
	  classes: {
	    base: {
			"color": "white",
			"white-space": "nowrap",
			"background-color": "#32ace9",
			"padding": "10px 20px",
			"opacity": "0.6",
			"border-radius": "5px"
	    },
	    superblue: {
	      "color": "white",
	      "background-color": "blue"
	    }
	  }
	});
	
	$.notify(message, {
	  style: 'blocked',
	  autoHideDelay: 2000,
	});
	
});

self.port.on('retrieveWord', function() {
	self.port.emit('addRecent', getWord());
});

self.port.on('toggleAudio', function(args) {
	var element = 'a[href$="' + args.url + '"]';
	changeSpeaker(element, args.state);
});

$('.audio-player.audio-player-hover').on( "ended", function(){
    console.log("Player stopped");
});

function checkMediaStatus() {
	
	var qqq = $('[class^="qquestion"]');
	
	if(qqq.length > 0){
		if(doesExist("word", getWord())){
			$(".hidden-audio.hide").attr('class', 'hidden-audio-disabled hide');
		}
		setCurrentRecent();
	}
	
	var foundMedia = new Array();

	blockedMedia.forEach(function(entry) {

		if (entry.type == "Audio") {
			var media = $('a[href="' + entry.url + '"]');
			if (media.length == 1) {
				foundMedia.push(entry);
			}
		} else if (entry.type == "Image") {
			var media = $('img[src="' + entry.url + '"]');
			if (media.length == 1) {
				foundMedia.push(entry);
			}
		}

	});
	if (foundMedia.length > 0) {
		disableMedia(foundMedia);
	} else {
		setCurrentRecent();
	}

}

function setCurrentRecent(){
	var hiddenAudio = $('.audio-player.audio-player-hover');
	if(hiddenAudio.length > 0){
		self.port.emit('setRecent', {"type": "Audio", "url": $(hiddenAudio).attr("href"), "word": getWord()});
	}
}

function removeBlock(url){

	type = "Audio";
	
	if (url.indexOf("uploads") !== -1 && url.indexOf("images") !== -1) {
		type = "Image";
	}
	
	if (type == "Audio") {
		var element = 'a[href$="' + url + '"]';
		changeSpeaker(element, true);
	} else if (type == "Image") {
		var element = 'img[src$="' + url + '"]';
		changeImage(element, true);
	}

}

function resetAll() {

	$( ".disabledAudio" ).each(function() {
		changeSpeaker(this, true);
	});
	
	$( ".disabledImage" ).each(function() {
		changeImage(this, true);
	});

}

function doesExist(property, value) {

	if (blockedMedia != null) {
		for (i = 0; i < blockedMedia.length; i++) {
			var entry = blockedMedia[i];
			if (entry[property] == value) {
				return true;
				break;
			}
		}
		return false;
	} else {
		return false;
	}
	
}

function disableMedia(foundMedia) {

	foundMedia.forEach(function(entry) {
		checkMedia(entry.type, entry.url);
	});

}

function checkMedia(type, url) {

	if (type == "Audio") {
		self.port.emit('setRecent', {"type": "Audio", "url": url, "word": getWord()});
		changeSpeaker($('a[href$="' + url + '"]'), false);
	} else if (type == "Image") {
		$('img[src$="' + url + '"]').css({ opacity: 0 });
		$('img[src$="' + url + '"]').attr('class', disabledImageClass);
	}

}


