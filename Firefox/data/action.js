self.on("click", function(node, data) {

	var word = getWord();
	var type;
	var url;
	if (node.src != undefined) {
		type = "Image";
		url = node.src;
	} else if (node.href != undefined) {
		type = "Audio";
		url = node.href;
	}

	var args = {
		"type": type,
		"url": url,
		"word": word
	};
	self.postMessage(args);
	toggleMedia(type, url);

});