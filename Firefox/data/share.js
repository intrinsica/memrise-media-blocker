
var enabledAudioClass = "audio-player audio-player-hover";
var disabledAudioClass = "disabledAudio";
var disabledAudioStyle = {
				"background-image": "url('http://static.memrise.com/img/sprites/icons-grey.png')",
				"background-position": "-192px -96px",
				"background-size": "224px 288px",
				"display": "block",
				"float": "right",
				"height": "32px",
				"margin-bottom": "0px",
				"margin-left": "10px",
				"margin-right": "0px",
				"margin-top": "0px",
				"padding-bottom": "0px",
				"padding-left": "0px",
				"padding-right": "0px",
				"padding-top": "0px",
				"text-decoration": "none",
				"width": "32px",
				"-moz-text-decoration-color": "#15A1EC",
				"-moz-text-decoration-line": "none",
				"-moz-text-decoration-style": "solid"
			};
var enabledImageClass = "enabledImage";
var disabledImageClass = "disabledImage";

function getWord() {

	var qqq = $('[class^="qquestion"]');
	
	if(qqq.length > 0){
		var word = $(qqq[0]).clone().children().remove().end().text().trim();
		return word;
	} else {
		var textArray = $(".row-value");
		if (textArray.length > 0) {
			var word = $(textArray[0]).clone().children().remove().end().text().trim();
			return word;
		} else {
			return "Unknown";
		}
	}

}

function toggleMedia(type, url) {

	if (type == "Audio") {
		
		var element = 'a[href$="' + url + '"]';
		var className = $(element).attr('class');
		
		if(className == disabledAudioClass) {
			changeSpeaker(element, true);
		} else {
			changeSpeaker(element, false);
		}

	} else if (type == "Image") {

		var element = 'img[src$="' + url + '"]';
		var className = $(element).attr('class');
		
		if(!className || className == enabledImageClass){
			changeImage(element, false);
		} else {
			changeImage(element, true);
		}
		
	}

}

function changeSpeaker(element, state) {

	if (state == false) {
		$(element).attr('class', disabledAudioClass);
		$(element).css(disabledAudioStyle);
	} else {
		$(element).attr('class', enabledAudioClass);
		$(element).css("background-position", "-64px -96px");
	}
	
}

function changeImage(element, state) {
	
	if (state == false) {
		$(element).fadeTo("fast", 0);
		$(element).attr('class', disabledImageClass);
	} else {
		$(element).fadeTo("fast", 100);
		$(element).attr('class', enabledImageClass);
	}
	
}
