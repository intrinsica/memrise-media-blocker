// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// A generic onclick callback function.

var menuID;
var contexts = ["link", "image"];

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.method === "getLocalStorage") {
		sendResponse({
			data: localStorage[request.key]
		});
	} else if (request.method === "toggleMedia") {
		chrome.tabs.query({currentWindow: true, active: true}, function(tabs){
			toggleMedia(tabs[0], request.url, true, getType(request.url));
		});
		sendResponse({});
	} else {
		sendResponse({});
	}
		
});

function genericOnClick(info, tab) {

	var linkURL;
	if(info["linkUrl"] != undefined) {
		linkURL = info["linkUrl"];
	} else {
		linkURL = info["srcUrl"];
	}
	chrome.tabs.sendMessage(tab.id, {
		text: "report_back",
		url: linkURL
	}, function(response) {

		var word = response.word;

		if (linkURL.indexOf("http://static.memrise.com/uploads/things") !== -1) {

			var blockArray = JSON.parse(localStorage.getItem('memriseBlock'));
			if(blockArray === null){
				blockArray = [];
			}
			
			var type = getType(linkURL);
			
			var entry = {
				"type": type,
				"url": linkURL,
				"word": word
			};
			
			if (doesExist(blockArray, linkURL)) {

				toggleMedia(tab, linkURL, true, type);
				remove(blockArray, linkURL);
				localStorage.setItem('memriseBlock', JSON.stringify(blockArray));
				chrome.contextMenus.update(menuID, {
					"title": "Block Media",
					"contexts": contexts,
					"onclick": genericOnClick
				});
				console.log("Media Unblocked.");

			} else {

				toggleMedia(tab, linkURL, false, type);
				blockArray.push(entry);
				localStorage.setItem('memriseBlock', JSON.stringify(blockArray));
				chrome.contextMenus.update(menuID, {
					"title": "Unblock Media",
					"contexts": contexts,
					"onclick": genericOnClick
				});
				console.log("Media Blocked.");

			}
			
		}

	});



}

chrome.webRequest.onBeforeRequest.addListener(
	
	function(details) {

		var linkURL = details.url;
		if (linkURL.indexOf("http://static.memrise.com/uploads/things") !== -1) {
			var blockArray = JSON.parse(localStorage.getItem('memriseBlock'));
			var type = getType(linkURL);
			if (doesExist(blockArray, linkURL)) {

				chrome.contextMenus.update(menuID, {
					"title": "Unblock Media",
					"contexts": contexts,
					"onclick": genericOnClick
				});

				chrome.tabs.query({
					currentWindow: true,
					active: true
				}, function(tabs) {
					tab = tabs[0];
					toggleMedia(tab, linkURL, false, type);

				});

				return {
					cancel: true
				};

			} else {

				chrome.contextMenus.update(menuID, {
					"title": "Block Media",
					"contexts": contexts,
					"onclick": genericOnClick
				});

				return {
					redirectUrl: linkURL
				};

			}
		}
	}, 
	{ urls: ["<all_urls>"]},
	["blocking"]
	
);
	
function toggleMedia(tab, link, state, type) {
	chrome.tabs.sendMessage(tab.id, {
		text: "toggle_media",
		state: state,
		url: link,
		type: type
	}, function(response) {});
	
}

function getType(url){
	
	if (url.indexOf("images") !== -1) {
		return "image";
	}  else {
		return "audio";
	}
	
}

function doesExist(arr, url) {

	if (arr != null) {
		for (i = 0; i < arr.length; i++) {
			var entry = arr[i];
			if (entry.url == url) {
				return true;
				break;
			}
		}
		return false;

	} else {
		return false;
	}

}

function remove(arr, url) {

	for (i = 0; i < arr.length; i++) {
		var entry = arr[i];
		if (entry.url == url) {
			arr.splice(i, 1);
			break;
		}
	}

}

menuID = chrome.contextMenus.create({
	"title": "Block Media",
	"contexts": ["link"],
	"onclick": genericOnClick
});

chrome.tabs.onActivated.addListener(function(activeInfo) {

    chrome.tabs.get(activeInfo.tabId, function (tab) {
        mySuperCallback(tab);
    });
	
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, updatedTab) {
    chrome.tabs.query({'active': true}, function (activeTabs) {
        var activeTab = activeTabs[0];

        if (activeTab == updatedTab) {
            mySuperCallback(activeTab);
        }
    });
});

function mySuperCallback(tab) {
    
	var newUrl = tab.url;
		
	if (newUrl.indexOf("www.memrise.com") !== -1) {	
		 chrome.browserAction.enable(tab.id)	 
	} else {
		 chrome.browserAction.disable(tab.id)
	}
	
}

