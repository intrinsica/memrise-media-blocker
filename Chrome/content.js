var entries;

chrome.runtime.sendMessage({method: "getLocalStorage", key: "memriseBlock"}, function(response) {
  entries = JSON.parse(response.data);
});

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {

    if (msg.text && (msg.text == "report_back")) {
	   	sendResponse({"word":getWord()});
    } else if (msg.text && (msg.text == "toggle_media")){
		checkLink(msg.url, msg.state, msg.type);
		sendResponse({});		
	}
	
});

function getWord() {

	var textArray = $(".row-value");
	if (textArray.length > 0) {
		var string = $(textArray[0]).text();
		return string.trim();
	} else {
		return "Unknown";
	}

}

function checkMedia(){
	
	for( var entry in entries){
		checkLink(entry.url, false, entry.type);
	}
	
}

function checkLink(url, state, type){
	console.log("U: " + url + " | S:" + state + " | T: " + type);
	if(type == "audio"){
		toggleAudio('a[href$="' + url + '"]', state)
	} else {
		toggleImages('img[src$="' + url + '"]', state)
	}
	
}

function toggleAudio(element, state){
	
	if(!state) {
		$(element).attr('class', 'disabledAudio');
		$(element).css({
			"background-image": "url('http://static.memrise.com/img/sprites/icons-grey.png')",
			"background-position": "-192px -96px",
			"background-size": "224px 288px",
			"display": "block",
			"height": "32px",
			"margin-bottom": "0px",
			"margin-left": "10px",
			"margin-right": "0px",
			"margin-top": "0px",
			"padding-bottom": "0px",
			"padding-left": "0px",
			"padding-right": "0px",
			"padding-top": "0px",
			"text-decoration": "none",
			"width": "32px",
			"-moz-text-decoration-color": "#15A1EC",
			"-moz-text-decoration-line": "none",
			"-moz-text-decoration-style": "solid"
		});
	} else {
		$(element).attr('class', 'audio-player audio-player-hover');
		$(element).css("background-position", "-64px -96px");
	}

}

function toggleImages(element, state){

	if(!state){
		$(element).fadeTo("fast", 0);
		$(element).attr('class', 'disabledImage');
	} else {
		$(element).fadeTo("fast", 100);
		$(element).attr('class', 'enabledImage');
	}
	
}

function doesExist(arr, url) {

	if (arr != null) {
		for (i = 0; i < arr.length; i++) {
			var entry = arr[i];
			if (entry.url == url) {
				return true;
				break;
			}
		}
		return false;
	} else {
		return false;
	}

}

$(document).bind("DOMSubtreeModified", function() {
	checkMedia();
});