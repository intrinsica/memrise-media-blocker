// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var entries;

function main() {
	
	entries = JSON.parse(localStorage.getItem('memriseBlock'));
	
	$('#tableContent').empty();
	
	var tbody = $('#tableContent'),
		props = ["*", "Media"];
		
	$.each(entries, function(i, entry) {
		var tag = "<div class='typeAudio' url='" + entry.url + "' ></div>";
		if (entry.type == "image") {
			tag = "<div class='typeImage' url='" + entry.url + "'  ></div>";
		} 
		
		var dir = oddOrEven(i);
		var rowType = "<tr>";
		if (dir == "odd") {
			rowType = "<tr class='pure-table-odd'>";
		}
		
		var tr = $(rowType);
		
		$("<td class='mediaType'>").html(tag).appendTo(tr);
		$("<td class='mediaData'>").html(entry.word).appendTo(tr);
		tbody.append(tr);
	});
	
	for(var i = entries.length; i<8; i++){
		var dir = oddOrEven(i);
		var rowType = "<tr>";
		if (dir == "odd") {
			rowType = "<tr class='pure-table-odd'>";
		}
		var tag = "<div class='typeBlank'></div>";
		var tr = $(rowType);
		$("<td class='mediaType'>").html(tag).appendTo(tr);
		$("<td class='mediaData'>").appendTo(tr);
		tbody.append(tr);
	}
	
	$(".typeAudio").mousedown(function() {
		var url = $(this).attr("url");
		console.log("Deleting:" + url);
		remove(entries, url);
		localStorage.setItem('memriseBlock', JSON.stringify(entries));
		chrome.runtime.sendMessage({method: "toggleMedia", url: url}, function(response) {});
		main();
		
	});
		
	$(".typeImage").mousedown(function() {
		var url = $(this).attr("url");
		console.log("Deleting:" + url);
		remove(entries, url);
		localStorage.setItem('memriseBlock', JSON.stringify(entries));
		chrome.runtime.sendMessage({method: "toggleMedia", url: url}, function(response) {});
		main();
		
	});
		
	$(".removeAction").mousedown(function() {
		entries.forEach(function(entry) {
			chrome.runtime.sendMessage({method: "toggleMedia", url: entry.url}, function(response) {});
			remove(entries, entry.url);
		});
		
		localStorage.setItem('memriseBlock', JSON.stringify(entries));
		main();
	});

}



function remove(arr, url) {

	for (i = 0; i < arr.length; i++) {
		var entry = arr[i];
		if (entry.url == url) {
			arr.splice(i, 1);
			break;
		}
	}
	
}

function oddOrEven(x) {
  return ( x & 1 ) ? "odd" : "even";
}

document.addEventListener('DOMContentLoaded', function() {
	main();
});
